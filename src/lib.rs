use glob::glob;
use std::path::PathBuf;

pub fn do_glob(path: &PathBuf) -> Vec<PathBuf> {
    let mut path = path.clone();
    let glob_pattern = path.as_mut_os_string();
    glob_pattern.push("/**/*.jpg");

    let globs = glob(glob_pattern.to_str().unwrap()).unwrap();
    let mut ok_paths = Vec::new();

    for path in globs {
        match path {
            Ok(path_buf) => ok_paths.push(path_buf),
            Err(ref e) => eprintln!("Error globbing {path:?}, {e:?}"),
        }
    }
    ok_paths
}

fn read_image_dimension(path: &PathBuf) -> f64 {
    let (img_width, img_height) = image::image_dimensions(path).unwrap();
    f64::from(img_width) / f64::from(img_height)
}

fn check_dimension(dim: f64, upper: f64, lower: f64) -> bool {
    (lower..=upper).contains(&dim)
}

pub fn is_wallpaper(path: &PathBuf, upper: f64, lower: f64) {
    let dim = read_image_dimension(path);
    if check_dimension(dim, upper, lower) {
        println!("{}", path.display());
    }
}
