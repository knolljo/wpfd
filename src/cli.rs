use std::path::PathBuf;

use clap::Parser;

/// Utility to find Wallpapers that fit into a ratio
#[derive(Parser)]
#[command(author, version, about)]
pub struct Cli {
    /// Directory to search pictures in [default: current working directory]
    #[arg()]
    pub path: Option<PathBuf>,

    /// The lower bound for a image ratio
    #[arg(long, short, default_value_t = 1.3)]
    pub lower_bound: f64,

    /// The upper bound for a image ratio
    #[arg(long, short, default_value_t = 2.5)]
    pub upper_bound: f64,
}
