use clap::Parser;
use std::{env, process};
use threadpool::ThreadPool;
mod cli;
use wallpaper_finder::{do_glob, is_wallpaper};

fn main() {
    let args = cli::Cli::parse();
    let img_paths = match args.path {
        Some(path) if path.exists() => do_glob(&path),
        Some(path) => {
            eprintln!("Path {path:?} does not exist");
            process::exit(1);
        }
        None => do_glob(&env::current_dir().unwrap()),
    };

    let pool = ThreadPool::new(24);
    for img_path in img_paths {
        pool.execute(move || is_wallpaper(&img_path, args.upper_bound, args.lower_bound));
    }

    pool.join();
}
